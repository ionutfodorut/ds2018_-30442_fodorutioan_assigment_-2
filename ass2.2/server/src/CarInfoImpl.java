import java.rmi.RemoteException;

public class CarInfoImpl implements CarInfo {

    @Override
    public double computeSellingPrice(Car c) throws RemoteException {
        if (c.getPurchasingPrice() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int year = c.getYear();
        long purchasingPrice = c.getPurchasingPrice();
        if (2018 - year < 7) {
            return purchasingPrice - (purchasingPrice / 7) * (2018 - year);
        }
        return 0;
    }

    @Override
    public double computeTax(Car c) throws RemoteException {
        // Dummy formula
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
