import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {

    public static void main(String args[]) {
            // Instantiating the implementation class
            CarInfoImpl implementation = new CarInfoImpl();
            // Exporting the object of implementation class
            // (here we are exporting the remote object to the stub)
            try {
                //System.setProperty("java.rmi.server.hostname","192.168.1.2");

                CarInfo stub = (CarInfo) UnicastRemoteObject.exportObject(implementation, 0);

                // Binding the remote object (stub) in the registry
                Registry registry = LocateRegistry.createRegistry(1099);

                registry.bind("CarInfo", stub);
            } catch (RemoteException ex) {
                System.err.println("Server exception: " + ex.toString());
                ex.printStackTrace();
                return;
            } catch (AlreadyBoundException ex) {
                ex.printStackTrace();
            }

        System.err.println("Server ready");

    }
}
