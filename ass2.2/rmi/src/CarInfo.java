
import java.rmi.Remote;
import java.rmi.RemoteException;

// Creating Remote interface for our application
public interface CarInfo extends Remote {
    double computeSellingPrice(Car c) throws RemoteException;

    double computeTax(Car c) throws RemoteException;
}