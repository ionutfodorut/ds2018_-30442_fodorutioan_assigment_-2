import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Home extends JFrame{
    private JButton button1;
    private JPanel panel1;
    private JTextField priceTextField;
    private JTextField engineTextField;
    private JTextField yearTextField;
    private JLabel Info;

    public Home()  {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Car c = new Car(Integer.parseInt(yearTextField.getText()), Integer.parseInt(engineTextField.getText()), Long.parseLong(priceTextField.getText()));

                try {

                    // Getting the registry
                    Registry registry = LocateRegistry.getRegistry(null);

                    // Looking up the registry for the remote object
                    CarInfo stub = (CarInfo) registry.lookup("CarInfo");

                    // Calling the remote method using the obtained object
                    double price = stub.computeSellingPrice(c);
                    double tax = stub.computeTax(c);

                    //System.out.println(price);
                    Info.setText("Price: " + price + " Tax: " + tax + ".");
                    // System.out.println("Remote method invoked");
                } catch (Exception ex) {
                    System.err.println("Client exception: " + ex.toString());
                    ex.printStackTrace();
                }
            }

        });
    }
}

